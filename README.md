# The Neco Arc Collection
A collection of my projects that involves Neco Arc in some way.<br>
I (ops400) do not own Neco Arc, please don't sue me.

---
## C Binary Char Converter
Prints "burenyuu" out a 2D array composed of the ASCII value of the chars in bits in backwards. **C Binary Char Converter** is actually a prototype for **Send & Receive Info**, you can see **C Binary Char Converter** code in send and receive code.
### Building
Run the ```make.sh``` file and execute the ```cbcc``` file, keep in mind that I only tested the ```make.sh``` script in Linux but I think it should work on Windows and MacOS too.
### That 2D array I talked about
```
unsigned int msg2Int2Byte[9][8] = {
{0,1,0,0,0,1,1,0},//01100010 b
{1,0,1,0,1,1,1,0},//01110101 u
{0,1,0,0,1,1,1,0},//01110010 r
{1,0,1,0,0,1,1,0},//01100101 e
{0,1,1,1,0,1,1,0},//01101110 n
{1,0,0,1,1,1,1,0},//01111001 y
{1,0,1,0,1,1,1,0},//01110101 u
{1,0,1,0,1,1,1,0},//01110101 u
{0,1,0,1,0,0,0,0}// 00001010 \n
};
```
neat, right?
## Send & Receive Info
Transmits "burenyuu" between one raspberry pi pico and another (RP2040 will do the work as long it has 9 available GPIOs) with an 8 bit connection and a sync button.
### "Setup"
             GPIO  5|btnpn|5  GPIO
             GND    |btnpn|    GND
             GPIO  6|-----|6  GPIO
             GPIO  7|-----|7  GPIO
             GPIO  8|-----|8  GPIO
             GPIO  9|-----|9  GPIO
    pico     GDN    |     |    GDN pico
    revciver GPIO 10|-----|10 GPIO sender
             GPIO 11|-----|11 GPIO
             GPIO 12|-----|12 GPIO
             GPIO 13|-----|13 GPIO
             btnpn = a button pin

So you are going connect the GPIO 5 from the receiver and sender to the same button pin and do the same to the ground of the sender and receiver to the same other button pin, I hope that helped.<br>
#### Here are some reference images
![reference image 1 for the wiring](./gallery/references/ref1.jpg "ref img 1")<br>
![reference image 2 for the wiring](./gallery/references/ref2.jpg "ref img 2")
### Building
You will need to have the [Arduino Pico Core by earlephilhower](https://github.com/earlephilhower/arduino-pico) installed, then just make sure that the wiring is correct and CPU Speed of both picos is set to 100MHz, after that upload receive info's sketch to one pico and the send info's sketch to the other pico.
### Usage
After building power up both picos at the same time, open the pico that the receive info sketch was uploaded to on serial monitor (like Arduino IDE own serial monitor), press the button and see the glorious "burenyuu" transmitted trough the 8 bit connection, if it doesn't transmit correctly press it again, this occurs by an error explained in the next section.
### One more note...
There is a 2 out of 2 margin of error, so for 1 "burenyuu" there is something like "bbvemnyuu", this error wont happen every time though, some times it's 1 wrong message to 7 right messages, but its not perfect sadly.
## ASCII Neco Arc (ANA)
Converts a 2D char array to a 3D uint8_t array that is structured like this:
```
uint8_t necoArcAscii2Byte[lines][char bit group][char's bits];
{{1,0,1,1,0,0,1,0},{1,0,1,1,0,0,1,0}}
```
This array is inverted (so 77 which is 01001101 is 10110010) so when converting back to decimal it's already in the right order and even if the number is only 7 bits it will occupy an 8 bit space because I feel it's more standardized this way. And finally that 3D array is converted to decimal in a 2D array and then displays this arrays values as if they were a char.
### Building/Running
For the demo 2 and the finalized **ASCII Neco Arc** run the ```make.sh``` script and run the newly created executable, for the demo 1 you can run the ```make.sh``` and run the executable for the C version, but for the web version (which is more "advanced", but was more a on the fly test, because I wrote it while I was doing some parallel work) you will need to run it on a browser.
### "Developer Cometary"
I thought that it would be some mind boggling thing, but in reality it was **C Binary Char Converter** with extras steps, in actuality **Send & Receive Info** was way harder to write, but **Send & Receive Info** problem was syncing the two rp picos to send info. In the future I may attempt to port this to **Send & Receive Info**, but first I have to make it use less wires maybe? Or have a better synchronization or a smaller ratio of wrong message to right message? Anyway for those wondering if the abbreviation ANA is a reference to someone, no its not it's just because I'm lazy to write "asciiNecoArc" in the file and folder creation.
### One more note...
I used [Online Ascii Art Creator](https://www.ascii-art-generator.org/) to make Neco Arc's ASCII thing and the image I think it was [this one](https://preview.redd.it/5wc934prt0r81.jpg?auto=webp&s=2eb141bf26da437fdd49834c5e843d5d31040508).

## raylib Rotating 3D Model (rlR3DM)
An rotating 3D model of Neco Arc made using the raylib library, I made this just to get my head of somethings I do not want to think now and see my friend's reaction.
### Building/Running
Run the ```make.sh``` script to compile the code and run the newly created ```rlR3DM``` executable.<br>**Side note on the build script:** This script only tested on Linux, so good luck Windows users to see if it works.
### "Developer Cometary"
I only made this project because I wanted to use raylib again and see if I could make my friend laugh with this and I did, but I don't know, I feel as even if I haven't done something wrong I feel like I did, what is wrong inside my mind? Or I'm just no a good interpreter?<br>Well anyways, one thing that i feel it was kinda clever was making a char array with the files path to sound effects and selecting them randomly.I'm actually quite impressed with this project, I really enjoy making it.
### 3D Model & Texture Credits
"Neco-arc" (https://skfb.ly/ooJzW) by Paper Bandit is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
### Side Notes
I modified a little bit the mainTexture.png to remove a purple dot, renamed the files, removed the ones I wasn't going to use and edited the original texture to make the Neco Arc Chaos'es texture.<br>Sorry for this not being a super detailed section like the others, I just don't have that much to say about this project.

## Coming Someday
When a bidirectional voltage converter arrives along side a 128x64 pixels display I will display "burenyuu" on a 1602 display with and without the I2C adapter and show a black and withe pixel art of Neco on the 128x64 display, but this projects with displays will take some time to I start, because the LCD and voltage converter are coming from China and I fear that they will be held in customs for a little while.<br>I'm trying to do an **ASCII Neco Arc** version using the **Send & Receive Info** code, but doing it the same way I did in the original **Send & Receive Info** would be ridiculous, so I will try to do a "for version" of **Send & Receive Info** and then with this for version I'll do an **ASCII Neco Arc** version of it.

## Update (16/08/2023)
Hello guys, I'm kinda demotivated to continue working on this, **Send & Receive Info**'s is not a good solution and I'm kinda procrastinating, doing things like making whole new projects and ricing my Linux, even tough I had to finish what I started, this was just a joke from the start, the screen thing probably will be done in another project and the ASCII art transferring maybe one day be done, but do not wait for me. For last the LCD 1602 thing for **Neco Arc Collection** will be abandoned too and maybe it will become some virtual pet idea I have had for some time now. Or who knows maybe one day I will finish what I've started.