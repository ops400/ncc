#include <raylib.h>
#include <stdio.h>
#include <stdint.h>
#define WW 1200
#define WH 720
#define WN "rlR3DM"
int main(){
    InitWindow(WW, WH, WN);
    SetTargetFPS(144);
    InitAudioDevice();
    SetMasterVolume(100);
    Model model = LoadModel("./src/res/mainModel.obj");
    const char *modelTexturesPath[2] = {"./src/res/mainTexture.png", "./src/res/mainTextureCHAOS.png"};
    Texture2D modelTexture = LoadTexture("./src/res/mainTexture.png");
    model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = modelTexture;
    Vector3 modelPos = {0.0f, -1.25f, 0.0f};
    Vector3 modelScale = {1.0f, 1.0f, 1.0f};
    Vector3 modelRotationAxis = {0.0f, 1.0f, 0.0f};
    uint16_t modelRotationAngle = 1;
    BoundingBox modelBound = GetMeshBoundingBox(model.meshes[0]);
    Sound necoArcSounds = LoadSound("./src/res/audio/neco-arc-dori.mp3");
    const char *necoArcSoundsFilePath[2][7] = {{
        "./src/res/audio/necoArc/neco-arc-dori.mp3", 
        "./src/res/audio/necoArc/neco-arc-pretty-boy.mp3", 
        "./src/res/audio/necoArc/neco-arc-sound-effect.mp3", 
        "./src/res/audio/necoArc/necoarc-loveanddeath.mp3", 
        "./src/res/audio/necoArc/necoarc-nyanyanya.mp3", 
        "./src/res/audio/necoArc/necoarc-nyanyanya2.mp3", 
        "./src/res/audio/necoArc/necoarc-nyeh.mp3"},{
        "./src/res/audio/necoArcChaos/big-sunday-neco-arc-chaos.mp3",
        "./src/res/audio/necoArcChaos/haha-neco-arc-chaos.mp3",
        "./src/res/audio/necoArcChaos/jyan-neco-arc-chaos.mp3",
        "./src/res/audio/necoArcChaos/nyan_morrendo-neco-arc-chaos.mp3",
        "./src/res/audio/necoArcChaos/sexy-neco-arc-chaos.mp3",
        "./src/res/audio/necoArcChaos/tori-neco-arc-chos.mp3",
        "./src/res/audio/necoArcChaos/itsu-magick-neco-arc-chaos.mp3"}};
    //0 Neco arc; 1 Neco Arc Chaos
    uint16_t shouldItBeNecoArcOrChaos = 0;
    Camera cam = {0};
    cam.fovy = 90.0f;
    cam.position = (Vector3){5.0f, 1.0f, 0.0f};
    cam.up = (Vector3){0.0f, 1.0f, 0.0f};
    cam.target = (Vector3){0.0f, 0.0f, 0.0f};
    cam.projection = CAMERA_PERSPECTIVE;
    Color rayWhiteClone = {245, 245, 245, 255};
    const Color backGroundColor = {34, 34, 34, 255};
    Vector2 previousMousePos = {0, 0};
    uint8_t shouldFpsApper = 0;
    uint8_t fpsY = 80;
    while(!WindowShouldClose()){
        if(IsKeyDown(KEY_EQUAL)) cam.fovy -= 0.5f;
        if(IsKeyDown(KEY_MINUS)) cam.fovy += 0.5f;
        if(GetMousePosition().x != previousMousePos.x || GetMousePosition().y != previousMousePos.y){
           previousMousePos = GetMousePosition();
           rayWhiteClone.a = 255; 
        }
        else if(rayWhiteClone.a > 0)rayWhiteClone.a--;
        if(IsKeyPressed(KEY_F) && shouldFpsApper == 0) shouldFpsApper = 1;
        else if(IsKeyPressed(KEY_F) && shouldFpsApper == 1) shouldFpsApper = 0;
        if(rayWhiteClone.a > 0) fpsY = 80;
        else fpsY = 0;
        if(IsKeyPressed(KEY_S)){
            UnloadSound(necoArcSounds);
            necoArcSounds = LoadSound(necoArcSoundsFilePath[shouldItBeNecoArcOrChaos][GetRandomValue(0, 6)]);
            PlaySound(necoArcSounds);
        }
        if(IsKeyPressed(KEY_C) && shouldItBeNecoArcOrChaos == 0){
            shouldItBeNecoArcOrChaos = 1;
            UnloadTexture(modelTexture);
            modelTexture = LoadTexture(modelTexturesPath[shouldItBeNecoArcOrChaos]);
            model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = modelTexture;
        }
        else if(IsKeyPressed(KEY_C) && shouldItBeNecoArcOrChaos == 1){
            shouldItBeNecoArcOrChaos = 0;
            UnloadTexture(modelTexture);
            modelTexture = LoadTexture(modelTexturesPath[shouldItBeNecoArcOrChaos]);
            model.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = modelTexture;
        }
        BeginDrawing();
            ClearBackground(backGroundColor);
            BeginMode3D(cam);
                DrawModelEx(model, modelPos, modelRotationAxis, modelRotationAngle, modelScale, WHITE);
            EndMode3D();
            DrawText("Press + to zoom in and - to zoom out", 0, 0, 20, rayWhiteClone);
            DrawText("Press F to show the current FPS", 0, 20, 20, rayWhiteClone);
            DrawText("Press S to play a random sound", 0, 40, 20, rayWhiteClone);
            DrawText("Press C to cycle between Neco Arc and Neco Arc Chaos", 0, 60, 20, rayWhiteClone);
            switch(shouldFpsApper){
                case 1:
                    DrawFPS(0,fpsY);
                    break;
                default:
                ;
            }
        EndDrawing();
        switch(modelRotationAngle){
            case 360:
                modelRotationAngle = 0;
                break;
            default:
             modelRotationAngle++;
        }
    }
    UnloadModel(model);
    UnloadTexture(modelTexture);
    CloseAudioDevice();
    CloseWindow();
    return 0;
}