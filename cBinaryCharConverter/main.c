#include <stdio.h>
#include <math.h>
#define BYTE 8

unsigned int msg2Int2Byte[9][8] = {
{0,1,0,0,0,1,1,0},//01100010 b
{1,0,1,0,1,1,1,0},//01110101 u
{0,1,0,0,1,1,1,0},//01110010 r
{1,0,1,0,0,1,1,0},//01100101 e
{0,1,1,1,0,1,1,0},//01101110 n
{1,0,0,1,1,1,1,0},//01111001 y
{1,0,1,0,1,1,1,0},//01110101 u
{1,0,1,0,1,1,1,0},//01110101 u
{0,1,0,1,0,0,0,0}// 00001010 \n
};
unsigned int msg2Int2Byte2Char[9] = {0,0,0,0,0,0,0,0,0};

int main(){
   for(int i = 0; i < 9; i++){
        for(int j = 0; j < BYTE; j++){
            msg2Int2Byte2Char[i] += msg2Int2Byte[i][j]*pow(2, j);
        }
        printf("%c",msg2Int2Byte2Char[i]);
    }
    return 0;
}

/*
MMMMMMMMMMMMWWMWWMWWMMMWNNNXxc:;;::;;:,lXWWMMMMMWMMMMMMMMMM
MMMMMMMMMMMMWWMWWWNXXXKkoldxdooddkOOkxccOXXXXXXXXXXNWMMMMMM
MMMMMMMMMMMWWWNKKOocloodddk0000KKKXKK0dcooollcclllldOXWMMMM
MMMMMMMMMMMWN0dcccloodkO0KKKXXXKKXXKKKOk0KKOxoooddddldNMMMM
MMMMMMWXOkkOxccodddxkOKKKKKK00O0KKXKKKKKKKKKKKOddx0OclXMMMM
MMMNOxOkdddxxxxddxkO00O0KK0xddx0KKKKKKK0OOKXKKOld0kodONWWMM
MMMXl;lxkkO0KKOxk0K0kxdO0OdldkOKKK0k0KX0xx0XKK0doocc0WMMMMM
MMMKllxOOkOkOK00KKXOox00OdldOXKKKkxdOKKOooxOKK0xl:dKNMMMMWM
MMMWXOodO00000KKKXXklx0Oxlo0KKKKOxddk0kkOxdx0KK0dckWWWMMWMM
MMMMMKoclxOOkxOKKK0kllxOkod0K00kkOkddkO0kk0xdO0KklOWMMMMMMM
MMMMMWN0dcloddOKK0xddxO0OcoK0kkO00OkOKNO,lXX0kkKklx0NWMMMMM
MMMMMMWWNd:oddkKK0kOKNWWO;cXNXK0000OXNWO,lNWNKOKOo:c0WMMMMM
MMMMWMWWNd;oddOKKKOKWWWW0:lXMMNK0KKKXXNXkONNKkxKOdcc0WMMMMM
MMMMMMMWXo:oddOKK0xxKNWWXO0NNKKKXXNNXKXNNNNX0lcxxdcc0WMMMMM
MMMMMMW0occoddk0K0ooOKKKXNNNXKKXNNXKX0xxKNNXOl;codcc0WMMMMM
MMMMMWWk;:lodddx0OooO000KXXX0OO0KKOxdddkKXXOo:,;cdcc0MMMMMM
MMMMMMWk;:lodoodOOlcoxO000KXXKK0kxxkO0KKKOxl;cc:co::0MMMMMM
MMMMMMWk;:cllcldkx:,;cldkkOKKKKKKXXKK0Oxoc;,:OO:;clxXMMMMWM
MMMMMMWk;:c:;,cooo:,,,,;:coxkO0KOxddlldo;,;lxXXkl;kWWMMMMMM
MMMMMMWKklcdkxo:cl;cxl;;;;;;codxxoc:;:oxxl:kWMWWXOKMMMMMMMM
MMMMMMMMWKKNMWXkolxKWXkl;ll:clokKK0Oxl::oxOXMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMWXKWMWNXocxkxOXXNWWWWNKxl:co0MMMMMMMMMMMMMMM
MMMMMMMMMMMMMMWMMMMMWOlldOkx0NNWWWWWWWXkc;:xXWMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWkcxOOkox0KNWWWWWWWKocoloKWMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMN0kxk00dcoO0NWWWWWWWXkolcoKMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMM0coO0OOxcoO0XNWWWWWWN0d:cdx0NWWMMMMMMMMM
MMMMMMMMMMMMMMMMMMW0coO0OOdcoOO00KXK0OOkdc::c:oKWWMMMMMMMMM
MMMMMMMMMMMMMMMMMWWOclxkxol:cllolll:;;;:;,,lxood0WMMMMMMMMM
MMMMMMMMMMMMMMMMMXxoox000Oko;'',;;,''',;;''lO0x:dWWMMMMMMMM
MMMMMMMMMMMMMMMWMXdlox0KK0xo;..,;,''';,,,'.:dkxokWWMMMMMMMM
MMMMMMMMMMMMMMMMMWN0xooxxl;'..'','..'',,.,clddkXNMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMWN0xddool;..:olloll''ckXWWWWWMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMWWWWMWWMWo.'kWMWWOl',KWWWWMWMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMWWMMWWW0l'.,kMMWNl. :KWWWMMMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMMWXxcl,...'OWNOc'..;0NNNNWMMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWk:'.'....,kKl,'..',:::;c0MMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWd..',,'';c0Kl,''',,,'''c0WMMMMMMMMMMMM
MMMMMMMMMMMMMMMMMMMMWd......oXXNWNd'.......oXWMMMMMMMMMMMMM
*/
