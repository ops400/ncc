#include <stdio.h>

void toBin(unsigned int n);
unsigned int outPut[8] = {0,0,0,0,0,0,0,0};

int main(){
    toBin(10);
    for (int i = 0; i < 8; i++){
        printf("%d", outPut[i]);
    }
    printf("\n");
    return 0;
}

void toBin(unsigned int n){
    unsigned int oldNumber = n;
    unsigned int calcPut[8] = {0,0,0,0,0,0,0,0};
    for(int i = 0; i < 8; i++){
        calcPut[i] = oldNumber%2;
        oldNumber = oldNumber/2;
        printf("%d;%d\n",calcPut[i],i);
    }
    outPut[0] = calcPut[7];
    outPut[1] = calcPut[6];
    outPut[2] = calcPut[5];
    outPut[3] = calcPut[4];
    outPut[4] = calcPut[3];
    outPut[5] = calcPut[2];
    outPut[6] = calcPut[1];
    outPut[7] = calcPut[0];
}