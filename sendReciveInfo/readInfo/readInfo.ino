#include <math.h>
#define BYTE 8
const uint bus1 = 13;
const uint bus2 = 12;
const uint bus3 = 11;
const uint bus4 = 10;
const uint bus5 = 9;
const uint bus6 = 8;
const uint bus7 = 7;
const uint bus8 = 6;
const uint sync = 5;
bool end = false;
uint msg2Int2Byte2Char[9] = {0,0,0,0,0,0,0,0,0};
uint msg2Int2Byte[9][8];
void setup() {
  Serial.begin(115200);
  pinMode(bus1, INPUT_PULLDOWN);
  pinMode(bus2, INPUT_PULLDOWN);
  pinMode(bus3, INPUT_PULLDOWN);
  pinMode(bus4, INPUT_PULLDOWN);
  pinMode(bus5, INPUT_PULLDOWN);
  pinMode(bus6, INPUT_PULLDOWN);
  pinMode(bus7, INPUT_PULLDOWN);
  pinMode(bus8, INPUT_PULLDOWN);
  pinMode(sync, INPUT_PULLUP);
}

void loop() {
  if(end == false){
    while(digitalRead(sync) == 1){
    }
    sleep_ms(100);
    msg2Int2Byte[0][0] = digitalRead(bus1);
    msg2Int2Byte[0][1] = digitalRead(bus2);
    msg2Int2Byte[0][2] = digitalRead(bus3);
    msg2Int2Byte[0][3] = digitalRead(bus4);
    msg2Int2Byte[0][4] = digitalRead(bus5);
    msg2Int2Byte[0][5] = digitalRead(bus6);
    msg2Int2Byte[0][6] = digitalRead(bus7);
    msg2Int2Byte[0][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[1][0] = digitalRead(bus1);
    msg2Int2Byte[1][1] = digitalRead(bus2);
    msg2Int2Byte[1][2] = digitalRead(bus3);
    msg2Int2Byte[1][3] = digitalRead(bus4);
    msg2Int2Byte[1][4] = digitalRead(bus5);
    msg2Int2Byte[1][5] = digitalRead(bus6);
    msg2Int2Byte[1][6] = digitalRead(bus7);
    msg2Int2Byte[1][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[2][0] = digitalRead(bus1);
    msg2Int2Byte[2][1] = digitalRead(bus2);
    msg2Int2Byte[2][2] = digitalRead(bus3);
    msg2Int2Byte[2][3] = digitalRead(bus4);
    msg2Int2Byte[2][4] = digitalRead(bus5);
    msg2Int2Byte[2][5] = digitalRead(bus6);
    msg2Int2Byte[2][6] = digitalRead(bus7);
    msg2Int2Byte[2][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[3][0] = digitalRead(bus1);
    msg2Int2Byte[3][1] = digitalRead(bus2);
    msg2Int2Byte[3][2] = digitalRead(bus3);
    msg2Int2Byte[3][3] = digitalRead(bus4);
    msg2Int2Byte[3][4] = digitalRead(bus5);
    msg2Int2Byte[3][5] = digitalRead(bus6);
    msg2Int2Byte[3][6] = digitalRead(bus7);
    msg2Int2Byte[3][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[4][0] = digitalRead(bus1);
    msg2Int2Byte[4][1] = digitalRead(bus2);
    msg2Int2Byte[4][2] = digitalRead(bus3);
    msg2Int2Byte[4][3] = digitalRead(bus4);
    msg2Int2Byte[4][4] = digitalRead(bus5);
    msg2Int2Byte[4][5] = digitalRead(bus6);
    msg2Int2Byte[4][6] = digitalRead(bus7);
    msg2Int2Byte[4][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[5][0] = digitalRead(bus1);
    msg2Int2Byte[5][1] = digitalRead(bus2);
    msg2Int2Byte[5][2] = digitalRead(bus3);
    msg2Int2Byte[5][3] = digitalRead(bus4);
    msg2Int2Byte[5][4] = digitalRead(bus5);
    msg2Int2Byte[5][5] = digitalRead(bus6);
    msg2Int2Byte[5][6] = digitalRead(bus7);
    msg2Int2Byte[5][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[6][0] = digitalRead(bus1);
    msg2Int2Byte[6][1] = digitalRead(bus2);
    msg2Int2Byte[6][2] = digitalRead(bus3);
    msg2Int2Byte[6][3] = digitalRead(bus4);
    msg2Int2Byte[6][4] = digitalRead(bus5);
    msg2Int2Byte[6][5] = digitalRead(bus6);
    msg2Int2Byte[6][6] = digitalRead(bus7);
    msg2Int2Byte[6][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[7][0] = digitalRead(bus1);
    msg2Int2Byte[7][1] = digitalRead(bus2);
    msg2Int2Byte[7][2] = digitalRead(bus3);
    msg2Int2Byte[7][3] = digitalRead(bus4);
    msg2Int2Byte[7][4] = digitalRead(bus5);
    msg2Int2Byte[7][5] = digitalRead(bus6);
    msg2Int2Byte[7][6] = digitalRead(bus7);
    msg2Int2Byte[7][7] = digitalRead(bus8);
    sleep_ms(100);
    msg2Int2Byte[8][0] = digitalRead(bus1);
    msg2Int2Byte[8][1] = digitalRead(bus2);
    msg2Int2Byte[8][2] = digitalRead(bus3);
    msg2Int2Byte[8][3] = digitalRead(bus4);
    msg2Int2Byte[8][4] = digitalRead(bus5);
    msg2Int2Byte[8][5] = digitalRead(bus6);
    msg2Int2Byte[8][6] = digitalRead(bus7);
    msg2Int2Byte[8][7] = digitalRead(bus8);
    for(int i = 0; i < 9; i++){
        for(int j = 0; j < BYTE; j++){
            msg2Int2Byte2Char[i] += msg2Int2Byte[i][j]*pow(2, j);
        }
        Serial.printf("%c",msg2Int2Byte2Char[i]);
    }
    Serial.printf("\n");
    for(uint i = 0; i < 9; i++){
      msg2Int2Byte2Char[i] = 0;
    }
    end = true;
  }
  sleep_ms(10);
  if(end == true && digitalRead(sync) == 1){
    end = false;
  }
}