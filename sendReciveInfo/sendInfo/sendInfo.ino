const uint bus1 = 13;
const uint bus2 = 12;
const uint bus3 = 11;
const uint bus4 = 10;
const uint bus5 = 9;
const uint bus6 = 8;
const uint bus7 = 7;
const uint bus8 = 6;
const uint sync = 5;
bool end = false;
const uint msg2Int2Byte[9][8] = {
{0,1,0,0,0,1,1,0},//01100010 b
{1,0,1,0,1,1,1,0},//01110101 u
{0,1,0,0,1,1,1,0},//01110010 r
{1,0,1,0,0,1,1,0},//01100101 e
{0,1,1,1,0,1,1,0},//01101110 n
{1,0,0,1,1,1,1,0},//01111001 y
{1,0,1,0,1,1,1,0},//01110101 u
{1,0,1,0,1,1,1,0},//01110101 u
{0,0,0,0,0,0,0,0}// 00001010 \n
};
void setup(){
  Serial.begin(115200);
  pinMode(bus1, OUTPUT);
  pinMode(bus2, OUTPUT);
  pinMode(bus3, OUTPUT);
  pinMode(bus4, OUTPUT);
  pinMode(bus5, OUTPUT);
  pinMode(bus6, OUTPUT);
  pinMode(bus7, OUTPUT);
  pinMode(bus8, OUTPUT);
  pinMode(sync, INPUT_PULLUP);
}

void loop(){
  if(end == false){
    while(digitalRead(sync) == 1){
    }
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[0][0]);
    digitalWrite(bus2, msg2Int2Byte[0][1]);
    digitalWrite(bus3, msg2Int2Byte[0][2]);
    digitalWrite(bus4, msg2Int2Byte[0][3]);
    digitalWrite(bus5, msg2Int2Byte[0][4]);
    digitalWrite(bus6, msg2Int2Byte[0][5]);
    digitalWrite(bus7, msg2Int2Byte[0][6]);
    digitalWrite(bus8, msg2Int2Byte[0][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[1][0]);
    digitalWrite(bus2, msg2Int2Byte[1][1]);
    digitalWrite(bus3, msg2Int2Byte[1][2]);
    digitalWrite(bus4, msg2Int2Byte[1][3]);
    digitalWrite(bus5, msg2Int2Byte[1][4]);
    digitalWrite(bus6, msg2Int2Byte[1][5]);
    digitalWrite(bus7, msg2Int2Byte[1][6]);
    digitalWrite(bus8, msg2Int2Byte[1][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[2][0]);
    digitalWrite(bus2, msg2Int2Byte[2][1]);
    digitalWrite(bus3, msg2Int2Byte[2][2]);
    digitalWrite(bus4, msg2Int2Byte[2][3]);
    digitalWrite(bus5, msg2Int2Byte[2][4]);
    digitalWrite(bus6, msg2Int2Byte[2][5]);
    digitalWrite(bus7, msg2Int2Byte[2][6]);
    digitalWrite(bus8, msg2Int2Byte[2][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[3][0]);
    digitalWrite(bus2, msg2Int2Byte[3][1]);
    digitalWrite(bus3, msg2Int2Byte[3][2]);
    digitalWrite(bus4, msg2Int2Byte[3][3]);
    digitalWrite(bus5, msg2Int2Byte[3][4]);
    digitalWrite(bus6, msg2Int2Byte[3][5]);
    digitalWrite(bus7, msg2Int2Byte[3][6]);
    digitalWrite(bus8, msg2Int2Byte[3][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[4][0]);
    digitalWrite(bus2, msg2Int2Byte[4][1]);
    digitalWrite(bus3, msg2Int2Byte[4][2]);
    digitalWrite(bus4, msg2Int2Byte[4][3]);
    digitalWrite(bus5, msg2Int2Byte[4][4]);
    digitalWrite(bus6, msg2Int2Byte[4][5]);
    digitalWrite(bus7, msg2Int2Byte[4][6]);
    digitalWrite(bus8, msg2Int2Byte[4][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[5][0]);
    digitalWrite(bus2, msg2Int2Byte[5][1]);
    digitalWrite(bus3, msg2Int2Byte[5][2]);
    digitalWrite(bus4, msg2Int2Byte[5][3]);
    digitalWrite(bus5, msg2Int2Byte[5][4]);
    digitalWrite(bus6, msg2Int2Byte[5][5]);
    digitalWrite(bus7, msg2Int2Byte[5][6]);
    digitalWrite(bus8, msg2Int2Byte[5][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[6][0]);
    digitalWrite(bus2, msg2Int2Byte[6][1]);
    digitalWrite(bus3, msg2Int2Byte[6][2]);
    digitalWrite(bus4, msg2Int2Byte[6][3]);
    digitalWrite(bus5, msg2Int2Byte[6][4]);
    digitalWrite(bus6, msg2Int2Byte[6][5]);
    digitalWrite(bus7, msg2Int2Byte[6][6]);
    digitalWrite(bus8, msg2Int2Byte[6][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[7][0]);
    digitalWrite(bus2, msg2Int2Byte[7][1]);
    digitalWrite(bus3, msg2Int2Byte[7][2]);
    digitalWrite(bus4, msg2Int2Byte[7][3]);
    digitalWrite(bus5, msg2Int2Byte[7][4]);
    digitalWrite(bus6, msg2Int2Byte[7][5]);
    digitalWrite(bus7, msg2Int2Byte[7][6]);
    digitalWrite(bus8, msg2Int2Byte[7][7]);
    sleep_ms(100);
    digitalWrite(bus1, msg2Int2Byte[8][0]);
    digitalWrite(bus2, msg2Int2Byte[8][1]);
    digitalWrite(bus3, msg2Int2Byte[8][2]);
    digitalWrite(bus4, msg2Int2Byte[8][3]);
    digitalWrite(bus5, msg2Int2Byte[8][4]);
    digitalWrite(bus6, msg2Int2Byte[8][5]);
    digitalWrite(bus7, msg2Int2Byte[8][6]);
    digitalWrite(bus8, msg2Int2Byte[8][7]);
    end = true;
  }
  sleep_ms(10);
  if(end == true && digitalRead(sync) == 1){
    end = false;
  }
}