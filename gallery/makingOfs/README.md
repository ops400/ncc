# raylib Rotating 3D Model (rlR3DM)
![A GIMP window with Neco Arc Chaos texture open and rlR3DM with rotation disabled](./rlR3DM_chaos_making_of_1.png "rlR3DM early version")<br>
![A GIMP window with Neco Arc Chaos texture open and rlR3DM with rotation disabled](./rlR3DM_chaos_making_of_2.png "rlR3DM early version")
# Send & Receive Info
![A photo of my hand holding the two picos after the first time they communicated correctly](./sendAndRecieveInfo_making_of.jpg "first successful communication")